# Import the required decorator and models
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Shoe, BinVO
import json
from common.json import ModelEncoder
# register the views!!!!!!! URL PATTERNS :D


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'image_url',
        'bin',
        'id',
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'image_url',
        'bin',
        'id',
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


# Define the view function and allow GET and POST requests
@require_http_methods(["GET", "POST"])
def list_shoes(request, bin_vo_id=None):
    # If the request method is GET
    if request.method == "GET":
        # If a bin ID is provided, filter shoes by bin ID
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        # Otherwise, return all shoes
        else:
            shoes = Shoe.objects.all()
        # Return a JSON response containing the shoes, encoded using a custom encoder
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    # If the request method is POST
    else:  # POST
        # Parse the request body as JSON
        content = json.loads(request.body)
        try:
            # Get the BinVO object with the specified bin ID
            bin_href = f'/api/bins/{content["bin"]}/'
            bin = BinVO.objects.get(import_href=bin_href)
            # Replace the bin ID in the content with the BinVO object
            content["bin"] = bin
        except BinVO.DoesNotExist:
            # If the BinVO object does not exist, return an error message
            return JsonResponse(
                {"error": "Bin does not exist SAD MESSAGE"},
                status=400
            )
        # Create a new Shoe object with the parsed content and the BinVO object as its bin attribute
        shoe = Shoe.objects.create(**content)
        # Return a JSON response containing the newly created Shoe object, encoded using the same custom encoder
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def shoe_details(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
        except Shoe.DoesNotExist:
            return JsonResponse({"message": 'Yup this is totally wrong... nice try.'})
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "y'all messed up. this clearly doesn't exist. be sad. fix it."}
            )
        Shoe.objects.filter(id=id).update(**content)

        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )












# from django.shortcuts import render
# from django.http import JsonResponse
# from django.views.decorators.http import require_http_methods
# import json

# from .Encoders import ShoeListEncoder, ShoeDetailEncoder, BinVOEncoder
# from .models import BinVO, Shoe


# # Create your views here.
# @require_http_methods(["GET", "POST"])
# def api_list_shoes(request):
#     if request.method == "GET":
#         shoes = Shoe.objects.all()
#         return JsonResponse(
#             {"shoes": shoes},
#             encoder=ShoeListEncoder,
#         )
#     else:
#         content = json.loads(request.body)
#         try:
#             bin_href = content["bin"]
#             print(bin_href)
#             bin = BinVO.objects.get(import_href=bin_href)
#             content["bin"] = bin
#         except BinVO.DoesNotExist:
#             return (JsonResponse({"message": "Invalid bin id"}),)
#         shoes = Shoe.objects.create(**content)
#         return JsonResponse(
#             shoes,
#             encoder=ShoeDetailEncoder,
#             safe=False,
#         )


# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_shoe_detail(request, pk):
#     if request.method == "GET":
#         try:
#             shoe = Shoe.objects.get(id=pk)
#             return JsonResponse(
#                 shoe,
#                 encoder=ShoeDetailEncoder,
#                 safe=False,
#             )
#         except Shoe.DoesNotExist:
#             response = JsonResponse({"message": "Shoe does not exist"})
#             response.status_code = 404
#             return response
#     elif request.method == "DELETE":
#         try:
#             shoe = Shoe.objects.get(id=pk)
#             shoe.delete()
#             return JsonResponse(
#                 shoe,
#                 encoder=ShoeDetailEncoder,
#                 safe=False,
#             )
#         except Shoe.DoesNotExist:
#             return JsonResponse({"message": "Shoe does not exist"})
#     else:
#         content = json.loads(request.body)
#         try:
#             if "bin" in content:
#                 bin_href = content["bin"]
#                 bin = BinVO.objects.get(import_href=bin_href)
#                 content["bin"] = bin
#         except BinVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "incorrect Bin location"},
#                 status=400,
#             )
#         Shoe.objects.filter(id=pk).update(**content)
#         response = Shoe.objects.get(id=pk)
#         return JsonResponse(
#             response,
#             encoder=ShoeDetailEncoder,
#             safe=False,
#         )


# @require_http_methods(["GET", "POST"])
# def api_bins(request):
#     if request.method == "GET":
#         bin = BinVO.objects.all()
#         return JsonResponse(
#             {"bins": bin},
#             encoder=BinVOEncoder,
#         )
