import React, { useEffect, useState } from "react";

function HatForm () {
    const[locations, setLocations] = useState([]);
    const[formData, setFormData] = useState({
        fabric: '',
        style: '',
        color: '',
        image_url: '',
        location: '',
    });

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch (hatsUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                fabric: '',
                style: '',
                color: '',
                image_url: '',
                location: '',
            });
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="my-5 container">
            <div className="row">
                <form onSubmit={handleSubmit} id="create-hat-form">
                    <h1 className="mb-3">
                        Super Fancy Hats. Make 'em. Or don't. I don't really care.
                    </h1>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.location} name="location" id="location" className="dropdownClasses" required>
                            <option value="">Choose a Location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>
                                        {location.closet_name}
                                    </option>
                                );
                                })}
                        </select>
                    </div>
                        <p className="mb-3">
                            Hat Stuff
                        </p>
                    <div className="row">
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.fabric} required placeholder="fabric" type="text" id="fabric" name="fabric" className="form-control"/>
                            <label htmlFor="name">Fabric</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.style} required placeholder="style" type="text" id="style" name="style" className="form-control"/>
                            <label htmlFor="style">Style Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.color} required placeholder="color" type="text" id="color" name="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.image_url} required placeholder="image_url" type="url" id="image_url" name="image_url" className="form-control"/>
                            <label htmlFor="url">Picture URL</label>
                        </div>
                    </div>
                    </div>
                    <button className="btn btn-lg btn-primary">Create!</button>
                </form>
            </div>
        </div>
    )
};

export default HatForm;
