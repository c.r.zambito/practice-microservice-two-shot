import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList'
import HatList from './HatList'
import ShoeForm from './ShoeForm'
import HatForm from './HatForm'
import UpdateShoeForm from './UpdateShoeForm'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="/hats" element={<HatList />} />
          <Route path="/add-shoe" element={<ShoeForm />} />
          <Route path="/add-hat" element={<HatForm />} />
          <Route path="/update-shoe" element={<UpdateShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
