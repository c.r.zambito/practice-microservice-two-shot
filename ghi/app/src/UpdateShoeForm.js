import React, { useState, useEffect } from "react";

function UpdateShoeForm({ shoe, onUpdate }) {
  const [formData, setFormData] = useState({
    manufacturer: shoe.manufacturer,
    model_name: shoe.model_name,
    color: shoe.color,
    image_url: shoe.image_url,
    bin: shoe.bin,
  });

  const [closetNames, setClosetNames] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8100/api/bins/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setClosetNames(data.bins);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleFormChange = (event) => {
    const inputName = event.target.name;
    const value = event.target.value;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = `http://localhost:8080/api/shoes/${shoe.id}/`;
    const config = {
      method: "PUT",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, config);

    if (response.ok) {
      const updatedShoe = await response.json();
      onUpdate(updatedShoe);
    }
  };

  return (
    <form onSubmit={handleSubmit} id="update-shoe-form">
      <div className="form-floating mb-3">
        <input
          onChange={handleFormChange}
          placeholder="Manufacturer"
          required
          type="text"
          name="manufacturer"
          id="manufacturer"
          className="form-control"
          value={formData.manufacturer}
        />
        <label htmlFor="manufacturer">Manufacturer</label>
      </div>
      <div className="form-floating mb-3">
        <input
          onChange={handleFormChange}
          placeholder="Model"
          required
          type="text"
          name="model_name"
          id="model_name"
          className="form-control"
          value={formData.model_name}
        />
        <label htmlFor="model_name">Model</label>
      </div>
      <div className="form-floating mb-3">
        <input
          onChange={handleFormChange}
          placeholder="Color"
          required
          type="text"
          name="color"
          id="color"
          className="form-control"
          value={formData.color}
        />
        <label htmlFor="color">Color</label>
      </div>
      <div className="form-floating mb-3">
        <input
          onChange={handleFormChange}
          placeholder="Picture URL"
          required
          type="text"
          name="image_url"
          id="image_url"
          className="form-control"
          value={formData.image_url}
        />
        <label htmlFor="image_url">Picture URL</label>
      </div>
      <div className="mb-3">
        <select
          value={formData.bin}
          onChange={handleFormChange}
          required
          name="bin"
          id="bin"
          className="form-select"
        >
          <option value="">Choose a Bin</option>
          {closetNames.map((closetName) => (
            <option value={closetName.id} key={closetName.id}>
              {closetName.closet_name}
            </option>
          ))}
        </select>
      </div>
      <button className="btn btn-primary">Update</button>
    </form>
  );
}

export default UpdateShoeForm;
